var library = 'Angela';
var path = require('path');
var CompressionPlugin = require('compression-webpack-plugin');
var CopyPlugin = require('copy-webpack-plugin');
var CleanWebpackPlugin = require('clean-webpack-plugin').CleanWebpackPlugin;
var ServiceWorkerWebpackPlugin = require('serviceworker-webpack-plugin');

module.exports = (env = 'development', arg) => {

	let config = {
		target: 'web',
		mode: env,
		plugins: [
			new CleanWebpackPlugin(),
			new ServiceWorkerWebpackPlugin({
				entry: path.join(__dirname, 'src/service-worker.js'),
				filename: 'service-worker.js'
			})
		],
		optimization: {
			minimizer: [],
		},
		entry: {
			Angela: path.resolve(__dirname, './src/Angela.js')
		},
		output: {
			path: path.resolve(__dirname, 'dist'),
			filename: '[name].bundle.js',
			library: library,
			libraryTarget: 'umd',
			libraryExport: 'default',
			umdNamedDefine: true
		},
		resolve: {
			alias: {
				helpers: path.resolve(__dirname, 'src/helpers'),
				libraries: path.resolve(__dirname, 'src/libraries'),
				restful: path.resolve(__dirname, 'src/RESTful-API')
			}
		},
		performance: {
			hints: process.env.NODE_ENV === 'production' ? 'warning' : false
		},
		module: {
			rules: [
				{
					test: /\.config$/,
					exclude: /(node_modules|bower_components)/,
					loader: 'raw-loader'
				},
				{
					test: /\.(ogg|mp3|wav|mpe?g)$/i,
					exclude: /(node_modules|bower_components)/,
					loader: 'url-loader',
					options: {
						esModule: false
					}
				}
			]
		}
	}

	if (env == 'production') {
		/**
		 * Compress files
		 */
		config.plugins.push(
			new CompressionPlugin({
				cache:true,
				deleteOriginalAssets:false
			})
		);

		/**
		 * copy .htaccess to output directory
		 */
		config.plugins.push(
			new CopyPlugin([
				{
					from : path.resolve(__dirname, './src/.htaccess'),
					to : path.resolve(__dirname, './dist/')
				}
			])
		);
	}

	return config;
}